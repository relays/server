## Server files

Repo containing all the python scripts needed to run the different servers for the relay apps.

### `NFCProxy`
- original relay apps: 
  * `CardEmulator` -- to be used against a real EMV reader; 
  * `TerminalEmulator` -- to be used against a card;
- requires the relay server `NFCProxy_relay.py`
 
```sh
usage: NFCProxy_relay.py [-h] [-tp TERMPORT] [-cp CARDPORT] TermEmulatorIP CardEmulatorIP

Relay server connecting to a terminal emulator and a card emulator

positional arguments:
  TermEmulatorIP        the IP of the Terminal Emulator APP
  CardEmulatorIP        the IP of the Card Emulator APP

optional arguments:
  -h, --help            show this help message and exit
  -tp TERMPORT, --termPort TERMPORT
                        the port for the Terminal Emulator APP (default: 59557)
  -cp CARDPORT, --cardPort CARDPORT
                        the port for the Card Emulator APP (default: 59556)
```

### `NFCProxy_uid`
- modified relay for Visa relay attack
- requires the relay server `NFCProxy_uid_relay.py`
- similar to `NFCProxy`
- it will read the card UID with the `TerminalEmulator` app, and send it to the
  `CardEmulator` via the relay server. `CardEmulator` will set the device UID
  to the received value
- `CardEmulator` is custom-tailored for a Nexus 5 (hammerhead) phone, running 
  Android 7.1


### Apple Replay and Relay
- requires the `proxmark_transport_firmware` to run on the Proxmark
- requires `CardEmulator` from the original `NFCProxy`
- requires one of the following python servers:
  * `apple_relay_full_visa.py` -- relays all APDUs, when requested by the EMV 
    reader;
  * `apple_full_relay_visa_limit_bypass.py` -- same as above, but bypasses 
    contactless limit;
  * `apple_relay_full_visa_read_records_first.py` -- relays all APDUs, but 
    performs all three READ RECORD commands when the reader sends its first one; 
    sends the replies back to the reader as requested;
  * `apple_relay_cached_records_visa.py` -- relays all APDUs up until the 
    READ RECORD commands; assumes the replies to these commands are provided in 
    the script;
  * `apple_relay_cached_records_visa_limit_bypass.py` -- same as above, but 
    bypasses contactless limit;
  

```sh
usage: apple_relay_full_visa.py [-h] [-p PORT] [-rp RP] pmPath hostIP

Relay for Apple Pay Transport mode Scenario: iPhone -> proxmark -> CardEMU -> terminal; to be used
with a Card Emulator APP

positional arguments:
  pmPath                the path to the proxmark client folder; excludes the pm3 executable
  hostIP                the IP of the Card Emulator APP

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  the port for the Card Emulator APP (default: 59556)
  -rp RP                The port number of the reader emulating proxmark; will be passed with the -n option (default: 1)
```

### Samsung Replay and Relay
- requires the `proxmark_transport_firmware` to run on the Proxmark
- requires `CardEmulator` from the original `NFCProxy`
- requires the relay server `samsung_full_relay_records_first.py`
- `-zs` flag to be used if the value of the transaction amount is to 
  be changed to zero (0)

```sh
usage: samsung_full_relay_records_first.py [-h] [-p PORT] [-rp RP] [-zs] pmPath hostIP

Relay for Samsung Pay Transport mode Scenario: Samsung Phone -> proxmark -> CardEMU -> terminal; to be used with a Card Emulator APP

positional arguments:
  pmPath                the path to the proxmark client folder; excludes the pm3 executable
  hostIP                the IP of the Card Emulator APP

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  the port for the Card Emulator APP (default: 59556)
  -rp RP                The port number of the reader emulating proxmark; will be passed with the -n option (default: 1)
  -zs                   zero sum; changes the amout to 0 (assumes at most £99.99 amount provided
                        (default: False)
```

