#!/usr/bin/python3

import sys

# This is a program to parse messages from Visa payWave and Mastercard PayPass contactless bank cards.
# It is based on code for contact based cards by Adam Laurie, http://rfidiot.org

# To use this program set message to equal the message from the card and type "python decodeEMV.py"
# You can parseEMV to parse messages.
#
# Tom Chothia tom.chothia@gmail.com





"""
General Error Codes
6400 	No specific diagnosis
6700 	Wrong length in Lc
6982 	Security status not satisfied
6985 	Conditions of use not satisfied
6a86 	Incorrect P1 P2
6d00 	Invalid instruction
6e00 	Invalid class
Install Load Errors
6581 	Memory Failure
6a80 	Incorrect parameters in data field
6a84 	Not enough memory space
6a88 	Referenced data not found
Delete Errors
6200 	Application has been logically deleted
6581 	Memory failure
6985 	Referenced data cannot be deleted
6a88 	Referenced data not found
6a82 	Application not found
6a80 	Incorrect values in command data
Get Data Errors
6a88 	Referenced data not found
Get Status Errors
6310 	More data available
6a88 	Referenced data not found
6a80 	Incorrect values in command data
Load Errors
6581 	Memory failure
6a84 	Not enough memory space
6a86 	Incorrect P1/P2
6985 	Conditions of use not satisfied 
"""

printASCII = False

#message="7081BD5A0849882404702360845F24031604309F46819073C36E57D5138F0D30EE7D836E20A5D6A64C958847646B6E907B5CCE22FD4990DE585D5BD7C7C914E5493D318775C468914853CABBEE7C4823211743A2E67B1138EFDEA5E6A51E8DC8371B6956F80A76939E06A9A30857A3A3127F595FEF54A19D27F180A6E7D90B8E0D6A9BFA936653B008EC8E242686E211C08E07FDB6085015019B4D3E8B08893119B1CBE98BCA279F4701039F480AE12E9D7FA2E1B5A44D659F6905016485E5B4"
#message = "6F 34 84 07 A0 00 00 00 03 10 10 A5 29 50 0A 56 69 73 61 20 44 65 62 69 74 87 01 02 9F 38 0C 9F 66 04 9F 02 06 9F 37 04 5F 2A 02 BF 0C 08 9F 5A 05 31 08 26 08 26 90 00".replace(" ", "")

BINARY= 0
TEXT= 1
BER_TLV= 2
NUMERIC= 3
MIXED= 4
TEMPLATE= 0
ITEM= 1
VALUE= 2
DOL=5
SFI= 0x88
CDOL1= 0x8c
CDOL2= 0x8d

AID = ''

# more tags at: https://www.eftlab.co.uk/index.php/site-map/knowledge-base/145-emv-nfc-tag
# add if needed

TAGS= 	{
	'4F':['Application Identifier (AID)',BINARY,ITEM], \
	'50':['Application Label',TEXT,ITEM], \
        '56':['Track 1 Equivalent Data',BINARY,ITEM], \
	'57':['Track 2 Equivalent Data',BINARY,ITEM], \
	'5A':['Application Primary Account Number (PAN)',NUMERIC,ITEM], \
        '5F2D':['Language Preference',NUMERIC,ITEM], \
        '61':['Directory Entry',BINARY,TEMPLATE], \
	'6F':['File Control Information (FCI) Template',BINARY,TEMPLATE], \
	'70':['Record Template',BINARY,TEMPLATE], \
	'77':['Response Message Template Format 2',BINARY,TEMPLATE], \
	'80':['Response Message Template Format 1',BINARY,ITEM], \
	'82':['Application Interchange Profile',BINARY,ITEM], \
	'83':['Command Template',BER_TLV,ITEM], \
#    '83':['Command Template',BINARY,TEMPLATE], \
	'84':['DF Name',MIXED,ITEM], \
	'86':['Issuer Script Command',BER_TLV,ITEM], \
	'87':['Application Priority Indicator',BER_TLV,ITEM], \
	'88':['Short File Identifier',BINARY,ITEM], \
	'8A':['Authorisation Response Code',BINARY,VALUE], \
	'8C':['Card Risk Management Data Object List 1 (CDOL1)',BINARY,DOL], \
	'8D':['Card Risk Management Data Object List 2 (CDOL2)',BINARY,DOL], \
	'8E':['Cardholder Verification Method (CVM) List',BINARY,ITEM], \
	'8F':['Certification Authority Public Key Index',BINARY,ITEM], \
	'91':['Issuer Authentication Data',BINARY,ITEM], \
	'93':['Signed Static Application Data',BINARY,ITEM], \
	'94':['Application File Locator',BINARY,ITEM], \
	'95':['Terminal Verification Results',BINARY,VALUE], \
	'97':['Transaction Certificate Data Object List (TDOL)',BER_TLV,ITEM], \
	'9C':['Transaction Type',BINARY,VALUE], \
	'9D':['Directory Definition File',BINARY,ITEM], \
	'5F20':['Cardholder Name',TEXT,ITEM], \
	'5F24':['Application Expiration Date YYMMDD',NUMERIC,ITEM], \
	'5F25':['Application Effective Date YYMMDD',NUMERIC,ITEM], \
	'5F28':['Issuer Country Code',NUMERIC,ITEM], \
	'5F2A':['Transaction Currency Code',BINARY,VALUE], \
	'5F2d':['Language Preference',TEXT,ITEM], \
	'5F30':['Service Code',NUMERIC,ITEM], \
	'5F34':['Application Primary Account Number (PAN) Sequence Number',NUMERIC,ITEM], \
	'5F50':['Issuer URL',TEXT,ITEM], \
        # '6700':['Wrong Length Indicator',None,None], \
        # '6985':['Conditions of Use Not Satisfied',None,None], \
        # '6A81':['Function Not Supported Indicator',None,None], \
        # '6A83':['File Not Found Indicator',None,None], \
        '90':['Issuer Public Key Certificate',BINARY,ITEM], \
	'92':['Issuer Public Key Remainder',BINARY,ITEM], \
	'9A':['Transaction Date',BINARY,VALUE], \
    '9000': ['Success code', None, None], \
	'9F01':['Acquirer Identifier',BINARY,VALUE], \
	'9F02':['Amount, Authorised (Numeric)',BINARY,VALUE], \
	'9F03':['Amount, Other (Numeric)',BINARY,VALUE], \
	'9F04':['Amount, Other (Binary)',BINARY,VALUE], \
	'9F05':['Application Discretionary Data',BINARY,ITEM], \
	'9F07':['Application Usage Control',BINARY,ITEM], \
	'9F08':['Application Version Number',BINARY,ITEM], \
	'9F09':['Application Version Number',BINARY,ITEM], \
        '9F0A':['Application Selection Registered Proprietary Data list',BINARY,ITEM], \
	'9F0D':['Issuer Action Code - Default',BINARY,ITEM], \
	'9F0E':['Issuer Action Code - Denial',BINARY,ITEM], \
	'9F0F':['Issuer Action Code - Online',BINARY,ITEM], \
	'9F10':['Issuer Application Data (IAD)',BINARY,ITEM], \
	'9F11':['Issuer Code Table Index',BINARY,ITEM], \
	'9F12':['Application Preferred Name',TEXT,ITEM], \
	'9F15':['Merchant Category Code (MCC)',BINARY,ITEM], \
    '9F7E':['Application Life Cycle Data', BINARY,ITEM], \
	'9F16':['Merchant Identifier',TEXT,ITEM], \
        '9F19':['Token Requestor ID',BINARY,ITEM], \
	'9F1A':['Terminal Country Code',BINARY,VALUE], \
	'9F1C':['Terminal Identification',TEXT,VALUE], \
    '9F1D':['Terminal Risk Management Data', BINARY,VALUE ], \
	'9F1E':['Interface Device (IFD) Serial Number',TEXT,VALUE], \
	'9F1F':['Track 1 Discretionary Data',TEXT,ITEM], \
	'9F20':['Track 2 Discretionary Data',TEXT,ITEM], \
        '9F21':['Transaction Time HHMMSS',TEXT,ITEM], \
	'9F24':['Payment Account Reference (PAR)',TEXT,ITEM], \
	'9F26':['Application Cryptogram',BINARY,ITEM], \
	'9F27':['Cryptogram Information Data',BINARY,ITEM], \
	'9F32':['Issuer Public Key Exponent',BINARY,ITEM], \
	'9F33':['Terminal Capabilities',BINARY,ITEM], \
        '9F34':['Cardholder Verification Method (CVM) Results',BINARY,ITEM], \
        '9F35':['Terminal Type',BINARY,ITEM], \
	'9F36':['Application Transaction Counter',BINARY,ITEM], \
	'9F37':['Unpredictable Number',BINARY,VALUE], \
	'9F6A':['Unpredictable Number (v2)',BINARY,ITEM], \
	'9F38':['Processing Options Data Object List (PDOL)',BINARY,DOL], \
	'9F39':['Point-of-Service (POS) Entry Mode',BINARY,VALUE], \
	'9F42':['Application Currency Code',NUMERIC,ITEM], \
	'9F44':['Application Currency Exponent',NUMERIC,ITEM], \
        '9F45':['Data Authentication Code',BINARY,ITEM], \
        '9F46':['ICC Public Key Cert',BINARY,ITEM], \
        '9F47':['ICC Public Key Expo',BINARY,ITEM], \
        '9F48':['ICC Public Key Reainder',BINARY,ITEM], \
        '9F49':['Dynamic Data Auth Object List (DDOL)',BINARY,DOL], \
	'9F4A':['Static Data Authentication Tag List',BINARY,ITEM], \
        '9F4B':['Signed Dynamic Application Data (SDAD)',BINARY,ITEM], \
        '9F4C':['ICC Dynamic Number',BINARY,ITEM], \
	'9F4D':['Log Entry',BINARY,ITEM], \
	'9F4E':['Merchant Name and Location',TEXT,ITEM], \
        '9F5A':['Application Program Identifier',BINARY,ITEM], \
	'9F66':[['Terminal Transaction Qualifier (TTQ)',BINARY,ITEM], ['PUNATC Track2 location',BINARY,ITEM]], \
        '9F69':{'A0000000031010': ['Card Authentication Related Data (nonce)',BINARY,ITEM], 'A0000000041010': ['Card Authentication Related Data (nonce)',BINARY,DOL]}, \
        '9F6C':['Card Transaction Qualifiers (CTQ)',BINARY,ITEM], \
        '9F6E':['Form Factor Indicator (qVSDC)',BINARY,ITEM], \
        '9F7C':['Customer Exclusive Data (CED)',BINARY,ITEM], \
        '9F62':['PCVC3 Track1 location',BINARY,ITEM], \
        '9F63':['PUNATC Track1 location',BINARY,ITEM], \
        '9F64':['NATC Track1 location',BINARY,ITEM], \
        '9F65':['PCVC3 Track2 location',BINARY,ITEM], \
        # '9F66':, \
        '9F67':['NATC Track2 location',BINARY,ITEM], \
        '9F6B':['Track1 data',BINARY,ITEM], \
    '9F5C':['Cumulative Total Transaction Amount Upper Limit',NUMERIC,ITEM], \
	'A5':['Proprietary Information',BINARY,TEMPLATE], \
	'99':['Transaction PIN',BINARY, ITEM], \
        '9F0B':['Cardholder name (extended)', BINARY, ITEM], \
	'BF0C':['File Control Information (FCI) Issuer Discretionary Data',BER_TLV,TEMPLATE], \
	'BF63':['Unknown Payment System Tag',None, ITEM], \
    'DF4B':['POS Cardholder Interaction Information', BINARY, ITEM], \
	'DF20':['Unknown Private Tag',None,None], \
    '9F5B':['Unknown (Issuer Script Results or DSDOL)', BINARY, ITEM], \
    '9F5C':['Unknown (Cumulative Total Transaction Amount Upper Limit (len6) or DS Requested Operator ID (len8) or Magstripe Data Object List (var) )', BINARY, ITEM], \
    '9F5D':['Unknown (Available Offline Spending Amount (len6) or Application Capabilities Information (len3))', BINARY, ITEM], \
    '9F5E':['Unknown (Consecutive Transaction International Upper Limit (var) or Data Storage ID (len8-11))', BINARY, ITEM], \
    '9F51':['DRDOL', BINARY, ITEM]
	}

# from https://trungvo.wordpress.com/2010/04/24/list-of-known-apdu-status-words/
# and (some of) https://www.eftlab.com/knowledge-base/complete-list-of-apdu-responses/
SW_CODES = {
    '6200': 'No information given (NV-Ram not changed)', \
    '6201': 'NV-Ram not changed 1.', \
    '6281': 'Part of returned data may be corrupted', \
    '6282': 'End of file/record reached before reading Le bytes', \
    '6283': 'Selected file invalidated', \
    '6284': 'Selected file is not valid. FCI not formated according to ISO', \
    '6285': 'No input data available from a sensor on the card. No Purse Engine enslaved for R3bc', \
    '62A2': 'Wrong R-MAC', \
    '62A4': 'Card locked (during reset( ))', \
    '62F1': 'Wrong C-MAC', \
    '62F3': 'Internal reset', \
    '62F5': 'Default agent locked', \
    '62F7': 'Cardholder locked', \
    '62F8': 'Basement is current agent', \
    '62F9': 'CALC Key Set not unblocked', \
    '6300': 'No information given (NV-Ram changed)', \
    '6381': 'File filled up by the last write. Loading/updating is not allowed.', \
    '6382': 'Card key not supported.', \
    '6383': 'Reader key not supported.', \
    '6384': 'Plaintext transmission not supported.', \
    '6385': 'Secured transmission not supported.', \
    '6386': 'Volatile memory is not available.', \
    '6387': 'Non-volatile memory is not available.', \
    '6388': 'Key number not valid.', \
    '6389': 'Key length is not correct.', \
    '63C0': 'Verify fail, no try left.', \
    '63C1': 'Verify fail, 1 try left.', \
    '63C2': 'Verify fail, 2 tries left.', \
    '63C3': 'Verify fail, 3 tries left.', \
    '63F1': 'More data expected.', \
    '63F2': 'More data expected and proactive command pending.', \
    '6400': 'No information given (NV-Ram not changed)', \
    '6401': 'Command timeout. Immediate response required by the card.', \
    '6500': 'No information given', \
    '6501': 'Write error. Memory failure. There have been problems in writing or reading the EEPROM. Other hardware problems may also bring this error.', \
    '6581': 'Memory failure', \
    '6600': 'Error while receiving (timeout)', \
    '6601': 'Error while receiving (character parity error)', \
    '6602': 'Wrong checksum', \
    '6603': 'The current DF file without FCI', \
    '6604': 'No SF or KF under the current DF', \
    '6669': 'Incorrect Encryption/Decryption Padding', \
    '6700': 'Wrong length', \
    '6800': 'No information given (The request function is not supported by the card)', \
    '6881': 'Logical channel not supported', \
    '6882': 'Secure messaging not supported', \
    '6883': 'Last command of the chain expected', \
    '6884': 'Command chaining not supported', \
    '6900': 'No information given (Command not allowed)', \
    '6901': 'Command not accepted (inactive state)', \
    '6981': 'Command incompatible with file structure', \
    '6982': 'Security condition not satisfied.', \
    '6983': 'Authentication method blocked', \
    '6984': 'Referenced data reversibly blocked (invalidated)', \
    '6985': 'Conditions of use not satisfied.', \
    '6986': 'Command not allowed (no current EF)', \
    '6987': 'Expected secure messaging (SM) object missing', \
    '6988': 'Incorrect secure messaging (SM) data object', \
    '698D': 'Reserved', \
    '6996': 'Data must be updated again', \
    '69E1': 'POL1 of the currently Enabled Profile prevents this action.', \
    '69F0': 'Permission Denied', \
    '69F1': 'Permission Denied – Missing Privilege', \
    '6A00': 'No information given (Bytes P1 and/or P2 are incorrect)', \
    '6A80': 'The parameters in the data field are incorrect.', \
    '6A81': 'Function not supported', \
    '6A82': 'File not found', \
    '6A83': 'Record not found', \
    '6A84': 'There is insufficient memory space in record or file', \
    '6A85': 'Lc inconsistent with TLV structure', \
    '6A86': 'Incorrect P1 or P2 parameter.', \
    '6A87': 'Lc inconsistent with P1-P2', \
    '6A88': 'Referenced data not found', \
    '6A89': 'File already exists', \
    '6A8A': 'DF name already exists.', \
    '6AF0': 'Wrong parameter value', \
    '6B00': 'Wrong parameter(s) P1-P2', \
    '6C00': 'Incorrect P3 length.', \
    '6D00': 'Instruction code not supported or invalid', \
    '6E00': 'Class not supported', \
    '6F00': 'Command aborted – more exact diagnosis not possible (e.g., operating system error).', \
    '6FFF': 'Card dead (overuse, …)', \
    '9000': 'Command successfully executed (OK).', \
    '9004': 'PIN not succesfully verified, 3 or more PIN tries left', \
    '9008': 'Key/file not found', \
    '9080': 'Unblock Try Counter has reached zero', \
    '9100': 'OK', \
    '9101': 'States.activity, States.lock Status or States.lockable has wrong value', \
    '9102': 'Transaction number reached its limit', \
    '910C': 'No changes', \
    '910E': 'Insufficient NV-Memory to complete command', \
    '911C': 'Command code not supported', \
    '911E': 'CRC or MAC does not match data', \
    '9140': 'Invalid key number specified', \
    '917E': 'Length of command string invalid', \
    '919D': 'Not allow the requested command', \
    '919E': 'Value of the parameter invalid', \
    '91A0': 'Requested AID not present on PICC', \
    '91A1': 'Unrecoverable error within application', \
    '91AE': 'Authentication status does not allow the requested command', \
    '91AF': 'Additional data frame is expected to be sent', \
    '91BE': 'Out of boundary', \
    '91C1': 'Unrecoverable error within PICC', \
    '91CA': 'Previous Command was not fully completed', \
    '91CD': 'PICC was disabled by an unrecoverable error', \
    '91CE': 'Number of Applications limited to 28', \
    '91DE': 'File or application already exists', \
    '91EE': 'Could not complete NV-write operation due to loss of power', \
    '91F0': 'Specified file number does not exist', \
    '91F1': 'Unrecoverable error within file', \
    '9210': 'Insufficient memory. No more storage available.', \
    '9240': 'Writing to EEPROM not successful.', \
    '9301': 'Integrity error', \
    '9302': 'Candidate S2 invalid', \
    '9303': 'Application is permanently locked', \
    '9400': 'No EF selected.', \
    '9401': 'Candidate currency code does not match purse currency', \
    '9402': 'Candidate amount too high', \
    '9402': 'Address range exceeded.', \
    '9403': 'Candidate amount too low', \
    '9404': 'FID not found, record not found or comparison pattern not found.', \
    '9405': 'Problems in the data field', \
    '9406': 'Required MAC unavailable', \
    '9407': 'Bad currency : purse engine has no slot with R3bc currency', \
    '9408': 'R3bc currency not supported in purse engine', \
    '9408': 'Selected file type does not match command.', \
    '9580': 'Bad sequence', \
    '9681': 'Slave not found', \
    '9700': 'PIN blocked and Unblock Try Counter is 1 or 2', \
    '9702': 'Main keys are blocked', \
    '9704': 'PIN not succesfully verified, 3 or more PIN tries left', \
    '9784': 'Base key', \
    '9785': 'Limit exceeded – C-MAC key', \
    '9786': 'SM error – Limit exceeded – R-MAC key', \
    '9787': 'Limit exceeded – sequence counter', \
    '9788': 'Limit exceeded – R-MAC length', \
    '9789': 'Service not available', \
    '9802': 'No PIN defined.', \
    '9804': 'Access conditions not satisfied, authentication failed.', \
    '9835': 'ASK RANDOM or GIVE RANDOM not executed.', \
    '9840': 'PIN verification not successful.', \
    '9850': 'INCREASE or DECREASE could not be executed because a limit has been reached.', \
    '9862': 'Authentication Error, application specific (incorrect MAC)', \
    '9900': '1 PIN try left', \
    '9904': 'PIN not succesfully verified, 1 PIN try left', \
    '9985': 'Wrong status – Cardholder lock', \
    '9986': 'Missing privilege', \
    '9987': 'PIN is not installed', \
    '9988': 'Wrong status – R-MAC state', \
    '9A00': '2 PIN try left', \
    '9A04': 'PIN not succesfully verified, 2 PIN try left', \
    '9A71': 'Wrong parameter value – Double agent AID', \
    '9A72': 'Wrong parameter value – Double agent Type', \
    '9D05': 'Incorrect certificate type', \
    '9D07': 'Incorrect session data size', \
    '9D08': 'Incorrect DIR file record size', \
    '9D09': 'Incorrect FCI record size', \
    '9D0A': 'Incorrect code size', \
    '9D10': 'Insufficient memory to load application', \
    '9D11': 'Invalid AID', \
    '9D12': 'Duplicate AID', \
    '9D13': 'Application previously loaded', \
    '9D14': 'Application history list full', \
    '9D15': 'Application not open', \
    '9D17': 'Invalid offset', \
    '9D18': 'Application already loaded', \
    '9D19': 'Invalid certificate', \
    '9D1A': 'Invalid signature', \
    '9D1B': 'Invalid KTU', \
    '9D1D': 'MSM controls not set', \
    '9D1E': 'Application signature does not exist', \
    '9D1F': 'KTU does not exist', \
    '9D20': 'Application not loaded', \
    '9D21': 'Invalid Open command data length', \
    '9D30': 'Check data parameter is incorrect (invalid start address)', \
    '9D31': 'Check data parameter is incorrect (invalid length)', \
    '9D32': 'Check data parameter is incorrect (illegal memory check area)', \
    '9D40': 'Invalid MSM Controls ciphertext', \
    '9D41': 'MSM controls already set', \
    '9D42': 'Set MSM Controls data length less than 2 bytes', \
    '9D43': 'Invalid MSM Controls data length', \
    '9D44': 'Excess MSM Controls ciphertext', \
    '9D45': 'Verification of MSM Controls data failed', \
    '9D50': 'Invalid MCD Issuer production ID', \
    '9D51': 'Invalid MCD Issuer ID', \
    '9D52': 'Invalid set MSM controls data date', \
    '9D53': 'Invalid MCD number', \
    '9D54': 'Reserved field error', \
    '9D55': 'Reserved field error', \
    '9D56': 'Reserved field error', \
    '9D57': 'Reserved field error', \
    '9D60': 'MAC verification failed', \
    '9D61': 'Maximum number of unblocks reached', \
    '9D62': 'Card was not blocked', \
    '9D63': 'Crypto functions not available', \
    '9D64': 'No application loaded', \
    '9E00': 'PIN not installed', \
    '9E04': 'PIN not succesfully verified, PIN not installed', \
    '9F00': 'PIN blocked and Unblock Try Counter is 3', \
    '9F04': 'PIN not succesfully verified, PIN blocked and Unblock Try Counter is 3', \
}

# CTQ
# 8000 (Byte 1 Bit 8) Online PIN required
# 4000 (Byte 1 Bit 7) Signature required
# 2000 (Byte 1 Bit 6) Go online if offline data auth fails and reader is online capable
# 1000 (Byte 1 Bit 5) Switch interface if offline data auth fails and reader suports VIS
# 0800 (Byte 1 Bit 4) Go online if application expired
# 0400 (Byte 1 Bit 3) Switch interface for cash
# 0200 (Byte 1 Bit 2) Switch interface for cashback
# 0080 (Byte 2 Bit 8) Consumer device CVM performed
# 0040 (Byte 2 Bit 7) Card supports issuer update processing at the POS
# AIP
# 4000 (Byte 1 Bit 7) SDA supported
# 2000 (Byte 1 Bit 6) DDA supported
# 1000 (Byte 1 Bit 5) Cardholder verification is supported
# 0800 (Byte 1 Bit 4) Terminal risk management is to be performed
# 0400 (Byte 1 Bit 3) Issuer authentication is supported
# 0100 (Byte 1 Bit 1) CDA supported
# 0080 (Byte 2 Bit 8) EMV and Magstripe Modes Supported
# 0040 (Byte 2 Bit 7) Expresspay Mobile supported

BITMASKS = {
    'Card Transaction Qualifiers (CTQ)': {
        'Online PIN required (Byte 1 Bit 8)': 0x8000, \
        'Signature required (Byte 1 Bit 7)': 0x4000, \
        'Go online if offline data auth fails and reader is online capable (Byte 1 Bit 6)': 0x2000, \
        'Switch interface if offline data auth fails and reader suports VIS (Byte 1 Bit 5)': 0x1000, \
        'Go online if application expired (Byte 1 Bit 4)': 0x0800, \
        'Switch interface for cash (Byte 1 Bit 3)': 0x0400, \
        'Switch interface for cashback (Byte 1 Bit 2)': 0x0200, \
        'Consumer device CVM performed (Byte 2 Bit 8)': 0x0080, \
        'Card supports issuer update processing at the POS (Byte 2 Bit 7)': 0x0040
    }, \
        'Application Interchange Profile': {
            'SDA supported (Byte 1 Bit 7)': 0x4000, \
            'DDA supported (Byte 1 Bit 6)': 0x2000, \
            'Cardholder verification is supported (Byte 1 Bit 5)': 0x1000, \
            'Terminal risk management is to be performed (Byte 1 Bit 4)': 0x0800, \
            'Issuer authentication is supported (Byte 1 Bit 3)': 0x0400, \
            'On device cardholder verification is supported (Byte 1 Bit 2)': 0x0200, \
            'CDA supported (Byte 1 Bit 1)': 0x0100, \
            'EMV and Magstripe Modes Supported (Byte 2 Bit 8)': 0x0080, \
            'Expresspay Mobile supported (Byte 2 Bit 7)': 0x0040, \
            'Relay resistance protocol is supported (Byte 2 Bit 1)': 0x0001
        }
}

CV_RULES = {
    ## this is the CVM Code
    'byte1': {
        0x00: 'Fail CVM processing', \
        0x01: 'Plaintext PIN verification performed by ICC', \
        0x02: 'Enciphered PIN verified online', \
        0x03: 'Plaintext PIN verification performed by ICC and signature (paper)', \
        0x04: 'Enciphered PIN verification performed by ICC', \
        0x05: 'Enciphered PIN verification performed by ICC and signature (paper)', \
        0x1e: 'Signature (paper)', \
        0x1f: 'No CVM required'
    },
    ## this is the CVM Condition Code
    'byte2': {
        '00': 'Always', \
        '01': 'If unattended cash', \
        '02': 'If not unattended cash and not manual cash and not purchase with cashback', \
        '03': 'If terminal supports the CVM', \
        '04': 'If manual cash', \
        '05': 'If purchase with cashback', \
        '06': 'If transaction is in the application currency 21 and is under X value', \
        '07': 'If transaction is in the application currency and is over X value', \
        '08': 'If transaction is in the application currency and is under Y value', \
        '09': 'If transaction is in the application currency and is over Y value', \

    }

}

def parseCVMList(cvm_list, indent):
    ## special case byte 1 bits 0-6 all 0
    ## 'Next': 0x40 mask
    ## 'Fail CVM processing': 0x3f
    ## eg 000000000000000042031E031F03
    print(" " * indent + 'Amount X:', cvm_list[:8])
    print(" " * indent + 'Amount Y:', cvm_list[8:16])
    n_rules = int((len(cvm_list) - 16) / 4)
    for _idx in range(n_rules):
        cv_rule = cvm_list[16 + _idx * 4:16 + _idx * 4 + 4]
        ## first byte
        next_rule = 'NEXT' if check_mask_set(int(cv_rule[:2], 16), 0x40) else 'FAIL'
        ## get rightmost 6 bits of code
        code = int(cv_rule[:2], 16) & 0x3f
        if not code in CV_RULES['byte1']:
            print(" " * indent + cv_rule[:2] + ' CVM Code not found, possible custom system')
        else:
            ## byte 2
            if not cv_rule[2:] in CV_RULES['byte2']:
                print(" " * indent + cv_rule[2:] + ' CVM Condition Code not found, possible custom system (if 0x80-0xFF)')
            else:
                print(" " * indent + cv_rule + ' ' + CV_RULES['byte1'][code] + ', ' + CV_RULES['byte2'][cv_rule[2:]] + ', ' + next_rule)



def parseEMV(m):


    if m[-4:] not in SW_CODES:
        print("Status code not found:", m[-4:])
    else:
        status_code = SW_CODES[m[-4:]]
        print("Status code:", m[-4:], status_code)
    # if m[-4:] == '9000':
    #     m = m[:-4]
    _pdol = []
    print_rep(m[:-4],0,_pdol)
    return _pdol

def check_mask_set(value_int, mask):
    if value_int & mask:
        return True
    else:
        return False

def check_mask_dict(value, mask_dict, indent):
    value_int = int(value, 16)
    # print("value int:", value_int)
    for key, mask in mask_dict.items():
        if value_int & mask:
            print(" " * indent + key)

def card_data(value, indent):
    ## fDDA version (byte 1), Card UN (bytes 2-5) and Card Transaction Qualifiers (bytes 6-7)
    ## 01755EF750 0000
    if AID == 'A0000000031010':
        print(' ' * indent, "fDDA version:", value[:2])
        print(' ' * indent, "Card UN:", value[2:10])
        print(' ' * indent, "CTQ:", value[10:])
        check_mask_dict(value[10:], BITMASKS['Card Transaction Qualifiers (CTQ)'], indent+4)
    elif AID == 'A0000000041010':
        print_rep(value, indent, [])
        

def print_rep(data,indent,pdol):
    global AID
    if len(data)==0: return ""
    found,tag,tagdata,rest = datatype_full(data)
    if (found):
        if tag == '9F69':
            tagdata = tagdata[AID]
        if (tagdata[2]==TEMPLATE):
            print(" "*indent+"%4s | len:"%str(tag)+rest[:2] +"    "+tagdata[0])
            this,next = take(rest)
            #print(str(this))
            #print(str(next))
            print_rep(this,indent+4,pdol)
            print_rep(next,indent+4,pdol)
        elif (tagdata[2]==ITEM):
            this,next = take(rest)
            taglength = findlength(rest)
            if tag == '4F':
                AID = this
            if (printASCII):
                print(" "*indent+"%4s | len:"%str(tag)+str(taglength) +"    "+tagdata[0]+": "+this+" "+this.decode("hex"))
            else:
                print(" "*indent+"%4s | len:"%str(tag)+str(taglength) +"    "+tagdata[0]+": "+this)
            ## if the tag is an Application File Locator:
            if tag == '94':
                parseAFL(this,True, indent+16)
            if tagdata[0] in BITMASKS:
                check_mask_dict(this, BITMASKS[tagdata[0]], indent+16)
            if tag == '8E':
                parseCVMList(this, indent+16)
            if tag == '9F69':
                card_data(this, indent+4)
            print_rep(next,indent,pdol)
        elif (tagdata[2]==DOL):
            this,next = take(rest)
            print(" "*indent+"%4s | len:"%str(tag)+rest[:2] +"    "+tagdata[0])
            _dol_structure = print_dol(this,indent+4)
            print_rep(next,indent,pdol)
            if len(_dol_structure.keys()) > 0:
                pdol.append(_dol_structure)
        else:
            this,next = take(rest)
            print("" * indent + "status: " + tagdata[0])
            print_rep(next,indent,pdol)
    else:
        print("not found: "+data)

def print_dol(data,indent):
    dol_structure = {}
    while (len(data)>0):
         found,tag,tagdata,rest = datatype_full(data)
         print(" "*indent +"%4s | len:"%str(tag)+rest[:2] +"   "+tagdata[0])
         dol_structure[tag] = int(rest[:2],16)
         data=rest[2:]
    return dol_structure     

def nthbyte(n,data):
   return data[n*2:n*2+2]

def hexlen(data):
   return "%02x"%(len(data)/2)

def datatype(data):
    if data[0:2] in TAGS.keys():
        return TAGS[data[0:2]]
    if data[0:4] in TAGS.keys():
        return TAGS[data[0:4]]
    return "Unknown TAG"


def datatype_full(data):
    if data[0:2] in TAGS.keys():
        return True,  data[0:2],TAGS[data[0:2]], data[2:]
    if data[0:4] in TAGS.keys():
        ## fix confusion between TTQ and PUNACT
        if data[0:4] == '9F66':
            dataLen = findlength(data[4:])
            if dataLen == 2:
                ## it's PUNATC
                for tag in TAGS[data[0:4]]:
                    if 'PUNATC' in tag[0]:
                        # print("**", data[0:4], tag, data[4:])
                        return True,  data[0:4], tag, data[4:]
            elif dataLen == 4:
                ## it's TTQ
                for tag in TAGS[data[0:4]]:
                    if 'TTQ' in tag[0]:
                        # print("**", data[0:4], tag, data[4:])
                        return True,  data[0:4], tag, data[4:]
            else: 
                return False, "Unknown len of 9F66 TAG " + data[0:2] + " or " + data[0:4], " ", data[4:]
        # print("*", data[0:4], TAGS[data[0:4]], data[4:])
        return True,  data[0:4], TAGS[data[0:4]], data[4:]
    return False, "Unknown TAG " + data[0:2] + " or " + data[0:4], " ", data[4:]

def take(data):
    var = data[0:2]
    intValue = int(var,16)
    blocksOfLength = 0
    if (intValue>128):
        blocksOfLength = intValue - 128
        length = 0
        for x in range(0, blocksOfLength):
            length = length+int(data[2+x*2:4+x*2],16)
    else:
        length = intValue
    return data[2+blocksOfLength*2:2+blocksOfLength*2+2*length],data[2+blocksOfLength*2+2*length:]


def takelen(len,data):
       return data[:2*int(len,16)],data[2*int(len,16):]

def brute(m):
    for i in range (0,len(m)/2):
        print_rep(m[i*2:],0,0)

def findlength(data):
    var = data[0:2]
    intValue = int(var,16)
    if (intValue>128):
        blocksOfLength = intValue - 128
        length = 0
        for x in range(0, blocksOfLength):
            length = length+int(data[2+x*2:4+x*2],16)
    else:
            length = intValue
    return length

def parseAFL (aflHexString,Print=True, indent=0):
    result = []
    noOfSFIs = (int)(len(aflHexString)/8)
    for x in range(0, noOfSFIs):
        offset = x * 8
        sfi = "{:02d}".format(int(aflHexString[offset + 0: offset + 2], 16) >> 3)
        firstRecordNo = aflHexString[offset + 2: offset + 4]
        lastRecordNo =  aflHexString[offset + 4: offset + 6]
        noOfflineAuth = aflHexString[offset + 6: offset + 8]
        if Print:
            print(" "*indent + "SFI: "+sfi+", 1st record: "+firstRecordNo+", last record: "+lastRecordNo+", no offline auth: "+noOfflineAuth)
        result.append((sfi,firstRecordNo,lastRecordNo,noOfflineAuth))
    
    return result

if __name__ == "__main__":
    parseEMV(sys.argv[1])

