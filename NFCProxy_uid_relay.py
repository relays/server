#!/usr/bin/env python3

"""
A server script that relays messages between a card and a terminal. Connects to
a client running the TerminalEmulator Android app, and a second client running
the CardEmulator Android app.
"""

import decodeEMV
import parseTerminalCommands

import argparse
import binascii
import datetime
import socket
import sys


def receive(s, print_resp=True):
    received_length = s.recv(4)
    length = int.from_bytes(received_length, byteorder='big')
    raw_response = s.recv(length)
    response = binascii.hexlify(bytearray(raw_response))
    fmt_response = response.decode('utf-8').upper()
    if print_resp:
        print(f"{fmt_response} ({datetime.datetime.utcnow()})")
    return fmt_response


def send(command, s):
    print("Sending " + command)
    command = command.upper()
    cmd_bytes = bytearray.fromhex(command)
    length = len(cmd_bytes)
    s.sendall(length.to_bytes(4, byteorder='big', signed=True))
    s.sendall(cmd_bytes)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Relay server connecting to a terminal emulator and a card emulator', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("TermEmulatorIP", help="the IP of the Terminal Emulator APP")
    parser.add_argument("-tp", "--termPort", type=int, default=59557, help="the port for the Terminal Emulator APP")
    parser.add_argument("CardEmulatorIP", help="the IP of the Card Emulator APP")
    parser.add_argument("-cp", "--cardPort", type=int, default=59556, help="the port for the Card Emulator APP")
    args = parser.parse_args()

    print("Attempting to connect..")
    print(f"cardEMU: {args.CardEmulatorIP} on port {args.cardPort}")
    print(f"termEMU: {args.TermEmulatorIP} on port {args.termPort}")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as term:
        term.connect((args.TermEmulatorIP, args.termPort))
        print(f"Connected to terminal emulator {args.TermEmulatorIP} on port {args.termPort}.")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as card:
            card.connect((args.CardEmulatorIP, args.cardPort))
            print(f"Connected to card emulator {args.CardEmulatorIP} on port {args.cardPort}.")
            _pdol = []
            ## First we get the UID, then relay
            while True:
                sys.stdout.write("Received command from terminal emulator: ")
                cmd = receive(term)
                if cmd == '':
                    print("Empty")
                _pdol = decodeEMV.parseEMV(cmd)
                send(cmd, card)

                sys.stdout.write("Received response from card emulator: ")
                resp = receive(card)
                if resp == '':
                    print("Empty")
                parseTerminalCommands.parseCommand(resp, PDOL=_pdol)
                send(resp, term)

            print("Finished.")
