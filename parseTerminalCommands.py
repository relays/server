from decodeEMV import TAGS, check_mask_dict, CV_RULES
import sys

# TTQ
# 800000 (Byte 1 Bit 8) MSD supported
# 200000 (Byte 1 Bit 6) qVSDC supported
# 100000 (Byte 1 Bit 5) EMV contact chip supported
# 080000 (Byte 1 Bit 4) Offline only reader
# 040000 (Byte 1 Bit 3) Online PIN supported
# 020000 (Byte 1 Bit 2) Signature supported
# 010000 (Byte 1 Bit 1) Offline data auth for online transactions supported
# 008000 (Byte 2 Bit 8) Online cryptogram required
# 004000 (Byte 2 Bit 7) CVM required
# 002000 (Byte 2 Bit 6) Contact chip offline pin supported
# 000080 (Byte 3 Bit 8) Issuer update processing supported
# 000040 (Byte 3 Bit 7) Mobile device functionality supported

BITMASKS = {
    'Terminal Transaction Qualifier (TTQ)': {
        'MSD supported (Byte 1 Bit 8)' : 0x80000000, \
        'EMV Mode supported (Byte 1 Bit 6)': 0x20000000,\
        'EMV contact chip supported (Byte 1 Bit 5)': 0x10000000,\
        'Offline only reader (Byte 1 Bit 4)': 0x08000000,\
        'Online PIN supported (Byte 1 Bit 3)': 0x04000000,\
        'Signature supported (Byte 1 Bit 2)': 0x02000000,\
        'Offline data auth for online transactions supported (Byte 1 Bit 1)': 0x01000000,\
        'Online cryptogram required (Byte 2 Bit 8)': 0x00800000,\
        'CVM required (Byte 2 Bit 7)': 0x00400000,\
        'Contact chip offline pin supported (Byte 2 Bit 6)': 0x00200000,\
        'Issuer update processing supported (Byte 3 Bit 8)': 0x00008000,\
        'Mobile device functionality supported (Byte 3 Bit 7)': 0x00004000, \
        'fDDA v1.0 Supported (Byte 4 Bit 8)': 0x00000080
    }
}

def parseCVMResults(cvm_res, indent):
    ## byte 1 -- CVM performed
    _str = ' ' * indent + cvm_res[:2]
    if cvm_res[:2] == '3F':
        _str += ' No CVM performed; \n' 
    else:
        code = int(cvm_res[:2], 16) & 0x3f
        if not code in CV_RULES['byte1']:
            _str += ' CVM Code not found, possible custom system; \n'
        else:
            _str += ' ' + CV_RULES['byte1'][code] + '; \n'
    ## byte 2 -- CVM condition
    _str += ' ' * indent + cvm_res[2:4]
    if cvm_res[2:4] == '00':
        _str += ' No CVM performed; \n'
    else:
        if not cvm_res[2:4] in CV_RULES['byte2']:
            _str += ' CVM Condition Code not found, possible custom system (if 0x80-0xFF); \n'
        else:
            _str += ' ' + CV_RULES['byte2'][cvm_res[2:4]] + '; \n'
    ## byte 3
    _str += ' ' * indent + cvm_res[4:]
    if cvm_res[4:] == '02':
        _str += ' Successful'
    elif cvm_res[4:] == '01':
        _str += ' Failed (or no CVM Condition Code satisfied)'
    elif cvm_res[4:] == '00':
        _str += ' Unknown'
    print(_str)


def parseTTQ(ttq):
    check_mask_dict(ttq, BITMASKS['Terminal Transaction Qualifier (TTQ)'], 4)

def parseReadRecord(cmd):
    sfi = int(cmd[6:8], 16) >> 3
    print("READ RECORD: "+cmd[4:6]+", SFI: "+"{:02d}".format(sfi))

def parseCommand (commandHex,Print=True,PDOL=[]):
    if (commandHex[0:8]=="00A40400"):
        if(commandHex[8:38]=="0E325041592E5359532E4444463031"):
            if Print: print("SELECT 2PAY.SYS.DDF01")
            return("SELECT",["2PAY.SYS.DDF01"])
        else:
            length = int(commandHex[8:10], 16)
            if Print: print("SELECT "+commandHex[10:(10+2*length)])
            return("SELECT",[commandHex[10:(10+2*length)]])
    ## GET PROCESSING OPTIONS
    elif (commandHex[0:4]=="80A8"):
    # elif (commandHex[0:2]=="80"):
        length = int(commandHex[8:10], 16)
        assert commandHex[10:12]=="83", "Incorrect GPO payload tag (not 83)"
        if Print: print("GPO command:")
        dataList = []
        if len(PDOL) > 0:
            ## we're assuming there is only one PDOL per message
            _pdol_dict = PDOL.pop(-1)
            commandIndex=14
            for dataTag, dataLength in _pdol_dict.items():
                ## fix TTQ / PUNACT confusion
                if dataTag == '9F66':
                    if dataLength == 2:
                        ## it's PUNATC
                        for tag in TAGS[dataTag]:
                            if 'PUNATC' in tag[0]:
                                if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {tag[0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}')
                                if tag[0] in BITMASKS:
                                    check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[tag[0]], 4)
                                dataList.append((dataTag, tag, commandHex[commandIndex:commandIndex+2*dataLength]))
                    elif dataLength == 4:
                        for tag in TAGS[dataTag]:
                            if 'TTQ' in tag[0]:
                                if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {tag[0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}')
                                if tag[0] in BITMASKS:
                                    check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[tag[0]], 4)
                                dataList.append((dataTag, tag, commandHex[commandIndex:commandIndex+2*dataLength]))
                    else:
                        print(f'Unknown length of tag 9F66')
                        return ("Unknown command",[])
                else:
                    if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {TAGS[dataTag][0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}') 
                    # if Print: print(" ",dataTag,":",commandHex[commandIndex:commandIndex+2*dataLength])
                    if TAGS[dataTag][0] in BITMASKS:
                        check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[TAGS[dataTag][0]], 4)
                    elif dataTag == '9F34':
                        parseCVMResults(commandHex[commandIndex:commandIndex+2*dataLength], 18)
                    dataList.append((dataTag, TAGS[dataTag][0], commandHex[commandIndex:commandIndex+2*dataLength]))
                commandIndex = commandIndex + 2*dataLength
        return("GPO",dataList)
    ## GENERATE AC
    elif (commandHex[0:4]=="80AE"):
        length = int(commandHex[8:10], 16)
        # assert commandHex[10:12]=="83", "Incorrect GPO payload tag (not 83)"
        if Print: print("Gen AC command:")
        dataList = []
        if len(PDOL) > 0:
            ## we're assuming there is only one PDOL per message
            _pdol_dict = PDOL.pop(-1)
            commandIndex=10
            for dataTag, dataLength in _pdol_dict.items():
                ## fix TTQ / PUNACT confusion
                if dataTag == '9F66':
                    if dataLength == 2:
                        ## it's PUNATC
                        for tag in TAGS[dataTag]:
                            if 'PUNATC' in tag[0]:
                                if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {tag[0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}')
                                if tag[0] in BITMASKS:
                                    check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[tag[0]], 4)
                                dataList.append((dataTag, tag, commandHex[commandIndex:commandIndex+2*dataLength]))
                    elif dataLength == 4:
                        for tag in TAGS[dataTag]:
                            if 'TTQ' in tag[0]:
                                if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {tag[0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}')
                                if tag[0] in BITMASKS:
                                    check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[tag[0]], 4)
                                dataList.append((dataTag, tag, commandHex[commandIndex:commandIndex+2*dataLength]))
                    else:
                        print(f'Unknown length of tag 9F66')
                        return ("Unknown command",[])
                else:
                    if Print: print(f' {dataTag: >4} | len {dataLength: >2}    {TAGS[dataTag][0]: <50}:{commandHex[commandIndex:commandIndex+2*dataLength]}') 
                    # if Print: print(" ",dataTag,":",commandHex[commandIndex:commandIndex+2*dataLength])
                    if TAGS[dataTag][0] in BITMASKS:
                        check_mask_dict(commandHex[commandIndex:commandIndex+2*dataLength], BITMASKS[TAGS[dataTag][0]], 4)
                    elif dataTag == '9F34':
                        parseCVMResults(commandHex[commandIndex:commandIndex+2*dataLength], 18)
                    dataList.append((dataTag, TAGS[dataTag][0], commandHex[commandIndex:commandIndex+2*dataLength]))
                commandIndex = commandIndex + 2*dataLength
        return("GEN AC")
    elif (commandHex[0:4]=="00B2"):
        sfi = int(commandHex[6:8], 16) >> 3
        if Print: print("READ RECORD: "+commandHex[4:6]+", SFI: "+"{:02d}".format(sfi))
        return ("READ RECORD",[commandHex[4:6],"{:02d}".format(sfi)])
    elif (commandHex[0:4]=="80EA"):
        if Print: print("RRP challenge: " + commandHex[4:])
        return("RRP challenge", [commandHex[4:]])
    else:
        if Print: print("Unknown command: "+commandHex)
        return ("Unknown command",[])


 
